﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Diagnostics;
using System.Web;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;

namespace MandatoryTwoServer
{
    public class ImagesController : ApiController
    {
        public HttpResponseMessage Get(string targetId, string imageId)
        {
            Console.WriteLine(@"Received request for image {0}\{1}", targetId, imageId);

            string path = string.Format(@"{0}spct_res\{1}\{2}", Path.GetTempPath(), targetId, imageId);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StreamContent(new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            return response;
        }

        public HttpResponseMessage Post()
        {
            Console.WriteLine("Incoming images...");

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = new MultipartMemoryStreamProvider();

            var task = Request.Content.ReadAsMultipartAsync(provider);
            task.Wait();

            string phoneId = string.Empty;
            var images = new List<SpctImage>();

            foreach (var content in provider.Contents)
            {
                var contentName = content.Headers.ContentDisposition.Name;
                contentName = contentName.Trim('\"');   // remove quotation marks from the name

                if (contentName == "id")
                {
                    var idTask = content.ReadAsStringAsync();
                    idTask.Wait();
                    phoneId = idTask.Result;
                }
                else
                {
                    var streamTask = content.ReadAsStreamAsync();
                    streamTask.Wait();
                    Bitmap image = new Bitmap(streamTask.Result);

                    /*** ALTERNATIVE METHOD ***/
                    //// If you would rather have a byte array, do this:
                    //var readBuffer = content.ReadAsByteArrayAsync();
                    //readBuffer.Wait();
                    //MemoryStream stream = new MemoryStream(readBuffer.Result);
                    //Bitmap image = new Bitmap(stream);

                    var spctImage = new SpctImage(contentName, image);

                    images.Add(spctImage);
                }
            }

            string storagePath = string.Format(@"{0}spct_res\{1}", Path.GetTempPath(), phoneId);
            Directory.CreateDirectory(storagePath);

            foreach (var spctImage in images)
            {
                var filename = string.Format(@"{0}\{1}", storagePath, spctImage.Name);
                spctImage.Image.Save(filename);
            }

            Console.WriteLine(string.Format("Saving {0} images from {1} at {2}", images.Count, phoneId, storagePath));
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
