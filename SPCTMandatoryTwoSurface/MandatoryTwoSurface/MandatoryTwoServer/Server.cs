﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Net.Http.Headers;

namespace MandatoryTwoServer
{
    public class Server
    {
        private const string SIGNALR_HOST_URL = "http://*:1337";
        private const string WEBAPI_HOST_URL = "http://*:9000";
        private static Server instance;
        Dictionary<string, string> phones;

        private Server()
        {
            phones = new Dictionary<string, string>();
            PhoneHub.OnClientConnected += OnClientConnected;
        }

        public void StartServer()
        {
            // start signalr server
            WebApp.Start<SignalRStartup>(SIGNALR_HOST_URL);
            Console.WriteLine("SignalR server running on {0}", SIGNALR_HOST_URL);

            // start web api server
            WebApp.Start<WebAPIStartup>(WEBAPI_HOST_URL);
            Console.WriteLine("WebAPI server running on {0}", WEBAPI_HOST_URL);
        }

        /// <summary>
        /// Event handler for event invoked in the hub.
        /// </summary>
        void OnClientConnected(string phoneId, string connectionId)
        {
            if (!phones.ContainsKey(phoneId))
            {
                Console.WriteLine("Client connected: {0} ({1})", phoneId, connectionId);
                phones.Add(phoneId, connectionId);
            }
            else
            {
                Console.WriteLine("Existing client connected: {0}. Updating connection id: {1}", phoneId, connectionId);
                phones[phoneId] = connectionId;
            }

            
            Console.WriteLine("Number of clients: {0}", phones.Count);
        }

        /// <summary>
        /// Request that a client downloads an image from the server.
        /// To be used when an image is dropped on a phone (on the Surface.)
        /// The URL should not include the base address.
        /// </summary>
        public void RequestDownloadImageFromServer(string clientId, string getURL, string targetId, string imageId)
        {
            var connectionId = phones[clientId];

            Console.WriteLine(@"Asking {0} to download {1}\{2} at {3}", clientId, targetId, imageId, getURL);
            GlobalHost.ConnectionManager.GetHubContext<PhoneHub>().Clients.Client(connectionId).downloadImageFromServer(getURL, targetId, imageId);
        }

        public static Server GetInstance()
        {
            if (instance == null)
            {
                instance = new Server();
            }

            return instance;
        }

        static void Main(string[] args)
        {
            Task.Run(() =>
                Server.GetInstance().StartServer()
            );

            Console.ReadLine();

            Task.Run(() =>
                Server.GetInstance().RequestDownloadImageFromServer("Mathias", "api/images", "Simon", "sample_2.jpg")
            );

            Console.ReadLine();
        }
    }

    class SignalRStartup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR();
        }
    }

    public class WebAPIStartup
    {
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{targetId}/{imageId}",
                defaults: new { targetId = RouteParameter.Optional, imageId = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));

            appBuilder.UseWebApi(config);
        }
    }
}
