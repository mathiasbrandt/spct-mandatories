﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MandatoryTwoServer
{
    class SpctImage
    {
        public SpctImage(string name, Bitmap image)
        {
            Name = name;
            Image = image;
        }

        public string Name { get; set; }
        public Bitmap Image { get; set; }
    }
}
