﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MandatoryTwoServer
{
    [HubName("PhoneHub")]
    public class PhoneHub : Hub
    {
        private const string postURL = "api/images";

        public static event Action<string, string> OnClientConnected = delegate { };
        public static event Action<string, string> OnUploadComplete = delegate { };
        public static event Action<string, string> OnDownloadComplete = delegate { };

        public void Identify(string phoneId)
        {
            //Console.WriteLine("New identification: " + phoneId);
            Clients.Caller.uploadImagesToServer(postURL);

            OnClientConnected(phoneId, Context.ConnectionId);
        }

        /// <summary>
        /// This is not used atm.
        /// </summary>
        /// <param name="phoneId"></param>
        public void UploadComplete(string phoneId)
        {
            Console.WriteLine(phoneId + " reports upload complete");
            OnUploadComplete(phoneId, Context.ConnectionId);
        }

        public void DownloadComplete(string phoneId)
        {
            Console.WriteLine(phoneId + " reports download complete");
            OnDownloadComplete(phoneId, Context.ConnectionId);
            Console.WriteLine("Download complete from phone ID: " + phoneId);
            Clients.Caller.uploadImagesToServer(postURL);
        }
    }
}
