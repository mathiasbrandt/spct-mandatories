﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Diagnostics;
using System.Web;
using System.Drawing;

namespace MandatoryTwoServer
{
    public class ValuesController : ApiController
    {
        // GET api/values 
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5 
        public string Get(int id)
        {
            return "value";
        }

        //// POST api/values 
        //public void Post([FromBody]string value)
        //{
        //    Request.Content
        //    Console.WriteLine("received value: {0}", value);
        //}

        //// PUT api/values/5 
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5 
        //public void Delete(int id)
        //{
        //}

        //public HttpResponseMessage Post()
        //{
        //    Console.WriteLine("Triggered!");

        //    if (!Request.Content.IsMimeMultipartContent())
        //    {
        //        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        //    }

        //    var provider = new MultipartMemoryStreamProvider();

        //    var task = Request.Content.ReadAsMultipartAsync(provider);
        //    task.Wait();

        //    foreach (var content in provider.Contents)
        //    {
        //        var streamTask = content.ReadAsStreamAsync();
        //        streamTask.Wait();
        //        var stream = streamTask.Result;
        //        Bitmap image = new Bitmap(stream);
        //        image.Save(@"C:\Users\brandt\Desktop\image.jpg");

        //        //var s = content.ReadAsByteArrayAsync();
        //        //s.Wait();
        //        //Console.WriteLine("Received {0} bytes", s.Result.Length);

        //        //var bytes = s.Result;
        //        //Bitmap image = Bitmap
        //        //Console.WriteLine("Received: " + s.Result);
        //    }

        //    //foreach (var file in provider.Contents)
        //    //{
        //    //    var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
        //    //    Task<byte[]> readBuffer = file.ReadAsByteArrayAsync();
        //    //    readBuffer.Wait();
        //    //    byte[] buffer = readBuffer.Result;
        //    //    // TODO: Do something with the buffer.
        //    //}

        //    return Request.CreateResponse(HttpStatusCode.OK);
        //}
    }
}