﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Threading;

namespace MandatoryTwoSurface
{
    public class PhonePictureModel
    {
        public ObservableCollection<string> ImagesOne { get; private set; }
        public ObservableCollection<string> ImagesTwo { get; private set; }

        //private string pathPhoneOne = @"C:\ITU\SPCT\spct-mandatories\SPCTMandatoryTwoSurface\MandatoryTwoSurface\MandatoryTwoSurface\Resources\PhoneOne\\";
        //private string pathPhoneTwo = @"C:\ITU\SPCT\spct-mandatories\SPCTMandatoryTwoSurface\MandatoryTwoSurface\MandatoryTwoSurface\Resources\PhoneTwo\\";



        private string pathPhoneOne = Path.GetTempPath() + "\\spct_res\\" + "Mathias\\";
        private string pathPhoneTwo = Path.GetTempPath() + "\\spct_res\\" + "Simon\\";
            // new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName + "\\Simon\\\\";
       // private string pathPhoneTwo = new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName + "\\Resources\\PhoneTwo\\";


        // Assumes that pictures are in the resources/phonex folder
        public PhonePictureModel()
        {
            ImagesOne = new ObservableCollection<string>();
            ImagesTwo = new ObservableCollection<string>();

            System.IO.Directory.CreateDirectory(pathPhoneOne);
            System.IO.Directory.CreateDirectory(pathPhoneTwo);

            watchPath(pathPhoneOne);
            watchPath(pathPhoneTwo);

            string[] images1 = Directory.GetFiles(pathPhoneOne, "*");
            string[] images2 = Directory.GetFiles(pathPhoneTwo, "*");

            foreach (var item in images1)
            {
                ImagesOne.Add(item);
            }

            foreach (var item in images2)
            {
                ImagesTwo.Add(item);
            }
        }

        private void watchPath(string path)
        {
            FileSystemWatcher watch = new FileSystemWatcher();
            watch.Path = path;

            watch.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            // Only watch text files.
            watch.Filter = "*";

            watch.Created += new FileSystemEventHandler(OnChanged);
            watch.Deleted += new FileSystemEventHandler(OnChanged);
            watch.Renamed += new RenamedEventHandler(OnRenamed);

            watch.EnableRaisingEvents = true;
        }


        private void OnChanged(object source, FileSystemEventArgs e)
        {
            string rootFolderPath = e.FullPath.Replace(System.IO.Path.GetFileName(e.FullPath), "");
            // Specify what is done when a file is changed, created, or deleted.
            if (rootFolderPath == pathPhoneOne)
            {
                switch (e.ChangeType)
                {
                    case WatcherChangeTypes.Created:
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => {ImagesOne.Add(e.FullPath); }));
                        break;

                    case WatcherChangeTypes.Deleted:
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => { ImagesOne.Remove(e.FullPath); }));
                        break;
                }


            }
            else if (rootFolderPath == pathPhoneTwo)
            {
                switch (e.ChangeType)
                {
                    case WatcherChangeTypes.Created:
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => { ImagesTwo.Add(e.FullPath); }));

                        break;

                    case WatcherChangeTypes.Deleted:
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => { ImagesTwo.Remove(e.FullPath); }));


                        break;
                }               
            }
            else
            {
                Console.WriteLine("File changed outside phone root folders");
            }
        }

        private void OnRenamed(object source, RenamedEventArgs e)
        {
            string rootFolderPath = e.FullPath.Replace(System.IO.Path.GetFileName(e.FullPath), "");
            if (rootFolderPath == pathPhoneOne)
            {
                Console.WriteLine("File renamed in the phone one folder");
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => { ImagesOne.Remove(e.OldFullPath); }));
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => { ImagesOne.Add(e.FullPath); }));



            }
            else if (rootFolderPath == pathPhoneTwo)
            {
                Console.WriteLine("File renamed in the phone one folder");
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => { ImagesTwo.Remove(e.OldFullPath); }));
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => { ImagesTwo.Add(e.FullPath); }));
            }
            else
            {
                Console.WriteLine("File renamed outside phone root folders");
            }
        }
    }


}
