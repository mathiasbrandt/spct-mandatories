using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;
using System.Collections.ObjectModel;
using System.Reflection;
using MandatoryTwoServer;

namespace MandatoryTwoSurface
{
    /// <summary>
    /// Interaction logic for SurfaceWindow1.xaml
    /// </summary>
    public partial class SurfaceWindow1 : SurfaceWindow
    {
        public PhonePictureModel model;
        private Server server;
        private string apiPath = "api/images";

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SurfaceWindow1()
        {
            server = Server.GetInstance();
            server.StartServer();
            InitializeComponent();
        }


        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            model = new PhonePictureModel();
            DataContext = model; // Important to set the datacontext to the PhonePictureModel for binding purposes

            ScatterViewOneLabel.Visibility = Visibility.Hidden;
            ScatterViewTwoLabel.Visibility = Visibility.Hidden;

            scatterViewOne.Visibility = Visibility.Hidden;
            scatterViewTwo.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Occurs when the window is about to close. 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }

        private void OnVisualizationAdded(object sender, TagVisualizerEventArgs e)
        {
            PhoneVisualization phone = (PhoneVisualization)e.TagVisualization;
            phone.LostTag += new RoutedEventHandler(OnLostTag);

            switch (phone.VisualizedTag.Value)
            {
                case 0x20:
                    phone.PhoneModel.Content = "Brandt's HTC One";
                    phone.myRect.Fill = SurfaceColors.Accent1Brush;
                    ToggleImageVisualizer(phone, Visibility.Visible);
                    ScatterViewOneLabel.Content = phone.PhoneModel.Content;
                    ScatterViewOneLabel.Visibility = Visibility.Visible;
                    break;
                case 0x21:
                    phone.PhoneModel.Content = "Simon's Apple 4s";
                    phone.myRect.Fill = SurfaceColors.Accent2Brush;
                    ToggleImageVisualizer(phone, Visibility.Visible);
                    ScatterViewTwoLabel.Content = phone.PhoneModel.Content;
                    ScatterViewTwoLabel.Visibility = Visibility.Visible;
                    break;
                default:
                    phone.PhoneModel.Content = "UNKNOWN MODEL";
                    phone.myRect.Fill = SurfaceColors.ControlAccentBrush;
                    break;
            }
        }

        private void OnLostTag(object sender, RoutedEventArgs e)
        {
            PhoneVisualization phone = (PhoneVisualization)e.Source;

            if (!phone.isPinned)
            {
                // Add an identical visualization to the scatterview at the same position
                phone.TagRemovedBehavior = TagRemovedBehavior.Fade;
                ToggleImageVisualizer(phone, Visibility.Hidden);
            }
        }

        // Attach a scatterview to a phone visualization
        private void ToggleImageVisualizer(PhoneVisualization phone, Visibility visibility)
        {
            ScatterView view;

            if (phone.VisualizedTag.Value == 0x20)
            {
                view = scatterViewOne;
                view.Visibility = visibility;
                ScatterViewOneLabel.Visibility = Visibility.Hidden;
            }
            else if (phone.VisualizedTag.Value == 0x21)
            {
                view = scatterViewTwo;
                view.Visibility = visibility;
                ScatterViewTwoLabel.Visibility = Visibility.Hidden;
            }
            else
            {
                // not a valid tag
            }



        }

        private void OnDragPreviewTouchDown(object sender, TouchEventArgs e)
        {
            FrameworkElement findSource = e.OriginalSource as FrameworkElement;
            ScatterViewItem draggedElement = null;

            // Find the touched SurfaceListBoxItem object.
            while (draggedElement == null && findSource != null)
            {
                if ((draggedElement = findSource as ScatterViewItem) == null)
                {
                    findSource = VisualTreeHelper.GetParent(findSource) as FrameworkElement;
                }
            }

            if (draggedElement == null)
            {
                return;
            }

            // Create the cursor visual.
            ContentControl cursorVisual = new ContentControl()
            {
                Content = draggedElement.DataContext,
                Style = FindResource("CursorStyle") as Style
            };

            // Add a handler. This will enable the application to change the visual cues.
            //SurfaceDragDrop.AddTargetChangedHandler(cursorVisual, OnTargetChanged);



            // Create a list of input devices. Add the touches that
            // are currently captured within the dragged element and
            // the current touch (if it isn't already in the list).
            List<InputDevice> devices = new List<InputDevice>();
            devices.Add(e.TouchDevice);
            foreach (TouchDevice touch in draggedElement.TouchesCapturedWithin)
            {
                if (touch != e.TouchDevice)
                {
                    devices.Add(touch);
                }
            }

            // Get the drag source object
            ItemsControl dragSource = ItemsControl.ItemsControlFromItemContainer(draggedElement);

            SurfaceDragCursor startDragOkay =
                SurfaceDragDrop.BeginDragDrop(
                  dragSource,                 // The SurfaceListBox object that the cursor is dragged out from.
                  draggedElement,             // The SurfaceListBoxItem object that is dragged from the drag source.
                  cursorVisual,               // The visual element of the cursor.
                  draggedElement.DataContext, // The data associated with the cursor.
                  devices,                    // The input devices that start dragging the cursor.
                  DragDropEffects.Move);      // The allowed drag-and-drop effects of the operation.

            // If the drag began successfully, set e.Handled to true. 
            // Otherwise SurfaceListBoxItem captures the touch 
            // and causes the drag operation to fail.
            e.Handled = (startDragOkay != null);

        }

        private void scatterViewOne_Drop(object sender, SurfaceDragDropEventArgs e)
        {
            string item = (string)e.Cursor.Data;
            if (!scatterViewOne.Items.Contains(item))
            {
                // Item was dropped in Mathias' scatterview, therefore send a request to his phone to download the file
               // string url = apiPath.Replace("phoneId", "Mathias").Replace("imageId", System.IO.Path.GetFileName(item));

                server.RequestDownloadImageFromServer("Mathias", apiPath, "Simon", System.IO.Path.GetFileName(item));
            }
        }

        private void scatterViewOne_DragLeave(object sender, SurfaceDragDropEventArgs e)
        {
            //insideOne = false;
        }

        private void scatterViewOne_DragEnter(object sender, SurfaceDragDropEventArgs e)
        {
            // insideOne = true;
        }

        private void scatterViewOne_DragCompleted(object sender, SurfaceDragCompletedEventArgs e)
        {
           
        }

        private void scatterViewTwo_Drop(object sender, SurfaceDragDropEventArgs e)
        {
            string item = (string)e.Cursor.Data;
            if (!scatterViewTwo.Items.Contains(item))
            {
                // Item was dropped in Simon's scatterview, therefore send a request to his phone to download the file
                // string url = apiPath.Replace("phoneId", "Simon").Replace("imageId", System.IO.Path.GetFileName(item));

                server.RequestDownloadImageFromServer("Simon", apiPath, "Mathias", System.IO.Path.GetFileName(item));
            }

        }

        private void scatterViewTwo_DragEnter(object sender, SurfaceDragDropEventArgs e)
        {

        }

        private void scatterViewTwo_DragLeave(object sender, SurfaceDragDropEventArgs e)
        {

        }

        private void scatterViewTwo_DragCompleted(object sender, SurfaceDragCompletedEventArgs e)
        {

        }


    }
}