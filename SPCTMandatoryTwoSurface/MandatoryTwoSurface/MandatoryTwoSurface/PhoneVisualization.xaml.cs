﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;
using System.Collections.ObjectModel;

namespace MandatoryTwoSurface
{
    /// <summary>
    /// Interaction logic for PhoneVisualization.xaml
    /// </summary>
    public partial class PhoneVisualization : TagVisualization
    {
        public Boolean isPinned { get; set; }

        public string phoneID { get; set; }

        public PhoneVisualization()
        {
            InitializeComponent();
        }

        private void PhoneVisualization_Loaded(object sender, RoutedEventArgs e)
        {
            //TODO: customize PhoneVisualization's UI based on this.VisualizedTag here
            if (this.VisualizedTag.Value == 20)
            {
                // This is brandt's phone
                phoneID = "Mathias";
            }
            else if (this.VisualizedTag.Value == 21)
            {
                // This is apple 4
                phoneID = "Simon";
            }
        }

        
        private void PhoneVisualization_Clicked(object sender, RoutedEventArgs e)
        {
            isPinned = !isPinned;
            SurfaceButton button = (SurfaceButton)e.Source;
            if (button.Content.ToString() == "Pin!")
            {
                button.Content = "Unpin!";
            }
            else
            {
                button.Content = "Pin!";
            }
        }
    }
}
