package dk.itu.spct.mandatory.one.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;

public class ProximityIntentReceiver extends BroadcastReceiver {

	private static final int NOTIFICATION_ID = 1337;
	private String name = "";
	private String IP = "";
	private TrackingActivity activity;

	public ProximityIntentReceiver(String name, String IP, TrackingActivity trackingActivity) {
		this.name = name;
		this.IP = IP;
		this.activity = trackingActivity;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// A user is now either entering or leaving the proximity zone.
		
		
		ClientSocket socket = new ClientSocket();
		String key = LocationManager.KEY_PROXIMITY_ENTERING; // Entering or exiting the zone?		
		Boolean entering = intent.getBooleanExtra(key, false);
		
		if (entering) {
			Log.d(getClass().getSimpleName(), "entering");
			Toast.makeText(context, "entering zone", Toast.LENGTH_LONG).show();
			socket.initialize(name, TrackingActivity.getBluetoothMacAddress(), true);
			socket.setIP(IP);
			socket.execute();
			sendNotification(context, name + " has entered ITU", "Entering ITU");
			activity.setBackground(true);
		} else {
			Log.d(getClass().getSimpleName(), "exiting");
			socket.initialize(name, TrackingActivity.getBluetoothMacAddress(), false);
			socket.setIP(IP);
			socket.execute();
			Toast.makeText(context, "exiting zone", Toast.LENGTH_LONG).show();
			sendNotification(context, name + " has left ITU", "Exiting ITU");
			activity.setBackground(false);
			
		}
		
		
		
	}
	
	/**
	 * Build and send a notification to the UI.
	 */
	private void sendNotification(Context context, String message, String title){
		Intent notificationIntent = new Intent(context, MainActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(context,
		        NOTIFICATION_ID, notificationIntent,
		        PendingIntent.FLAG_CANCEL_CURRENT);

		NotificationManager nm = (NotificationManager) context
		        .getSystemService(Context.NOTIFICATION_SERVICE);

		Notification.Builder builder = new Notification.Builder(context);

		
		Resources res = context.getResources();
		
		builder.setContentIntent(contentIntent)
					.setSmallIcon(android.R.drawable.ic_menu_call)
					.setLargeIcon(BitmapFactory.decodeResource(res, android.R.drawable.btn_dialog))	
					.setTicker(message)
		            .setWhen(System.currentTimeMillis())
		            .setAutoCancel(true)
		            .setContentTitle(title)
		            .setContentText(message);
		Notification n = builder.build();

		nm.notify(NOTIFICATION_ID, n);
	}

}