package dk.itu.spct.mandatory.one.app;


import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;

import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class ClientSocket extends AsyncTask<Void, Void, Void> {
	private static Socket socket;
	private String name;
	private String bluetoothID;
	private boolean isArriving;
	private String host;
	
	public ClientSocket(){
		
	}
	
	public void initialize(String name, String bluetoothID, boolean isArriving){
		host = "10.25.215.245";
		this.name = name;
		this.bluetoothID = bluetoothID;
		this.isArriving = isArriving;
	}
	
	public void sendMessage(){
		try
        {
            int port = 1337;
            InetAddress address = InetAddress.getByName(host);
            socket = new Socket(address, port);
 
            //Send the message to the server
            OutputStream os = socket.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);
            
            JSONObject JSONMessage = new JSONObject();
            JSONMessage.put("name", this.name);
            JSONMessage.put("bluetoothId", this.bluetoothID);
            JSONMessage.put("isArriving", isArriving);
            
            bw.write(JSONMessage.toString());
            bw.flush();
            Log.d("Socket", "Message sent to the server : " + JSONMessage.toString());
 
        }
        catch (Exception exception) 
        {
            exception.printStackTrace();
        }
        finally
        {
            // Closing the socket
            try
            {
                socket.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
	}

	@Override
	protected Void doInBackground(Void... params) {
		sendMessage();
		return null;
	}

	public void setIP(String IP) {
		host = IP;
	}
	
	
}
