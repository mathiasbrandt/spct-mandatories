package dk.itu.spct.mandatory.one.app;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	public static final String MAIN_ACTIVITY_NAME_FIELD = "main_activity_name_field";
	public static final String MAIN_ACTIVITY_IP_ADDRESS = "main_activity_ip_address";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button GO = (Button) findViewById(R.id.btn_start_tracking);
		GO.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this, TrackingActivity.class);
				
				TextView ip = (TextView) MainActivity.this.findViewById(R.id.main_txt_socket_addr);
				TextView txt = (TextView) MainActivity.this.findViewById(R.id.main_txt_name);
				i.putExtra(MAIN_ACTIVITY_NAME_FIELD, txt.getText().toString());
				i.putExtra(MAIN_ACTIVITY_IP_ADDRESS, ip.getText().toString());
				
				startActivity(i);
				
			}
		});
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}   
}
