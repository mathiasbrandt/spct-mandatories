package dk.itu.spct.mandatory.one.app;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Criteria;
import android.location.LocationListener;
import android.location.LocationManager;

/**
*
 */
public class LocationFinder {

	protected static String TAG = "LocationFinder";
	protected static String SINGLE_LOCATION_UPDATE_ACTION = "dk.itu.spct.mandatory.one.app.SINGLE_LOCATION_UPDATE_ACTION";

	protected PendingIntent singleUpatePI;
	protected LocationListener locationListener;
	protected LocationManager locationManager;
	protected Context context;
	protected Criteria criteria;
	

    private static final long POINT_RADIUS = 20; // in Meters
    private static final long PROX_ALERT_EXPIRATION = -1;
    
    private static final String PROX_ALERT_INTENT = "dk.itu.spct.mandatory.one.app.proxAlert";
    
    private ProximityIntentReceiver receiver;
    
	/**
	 * Construct a new Last Location Finder.
	 * This will add a receiver that listens to location updates
	 * 
	 * @param context
	 *            Context
	 * @param IP 
	 * @param trackingActivity 
	 */
	public LocationFinder(Context context, String name, String IP, TrackingActivity trackingActivity) {
		this.context = context;
		locationManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		// Coarse accuracy is specified here to get the fastest possible result.
		criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_LOW);

		// Construct the Pending Intent that will be broadcast by the
		// location update.
		Intent updateIntent = new Intent(SINGLE_LOCATION_UPDATE_ACTION);
		singleUpatePI = PendingIntent.getBroadcast(context, 0, updateIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		
		receiver = new ProximityIntentReceiver(name, IP, trackingActivity);
	}

	/**
	 * 
	 * @param latitude
	 * @param longitude
	 */
	protected void addProximityAlert(double latitude, double longitude) {
		Intent intent = new Intent(PROX_ALERT_INTENT);
		PendingIntent proximityIntent = PendingIntent.getBroadcast(context, 0,
				intent, 0);
		locationManager.addProximityAlert(latitude, // the latitude of the
													// central point of the
													// alert region
				longitude, // the longitude of the central point of the alert
							// region
				POINT_RADIUS, // the radius of the central point of the alert
								// region, in meters
				PROX_ALERT_EXPIRATION, // time for this proximity alert, in
										// milliseconds, or -1 to indicate no
										// expiration
				proximityIntent // will be used to generate an Intent to fire
								// when entry to or exit from the alert region
								// is detected
				);
		IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT);
		context.registerReceiver(receiver, filter);

	}
	
	
	/**
	 * Unregister the ProximityIntentReceiver to avoid leaking the receiver.
	 */
	public void unRegisterReceiver(){
		context.unregisterReceiver(receiver);
	}
}