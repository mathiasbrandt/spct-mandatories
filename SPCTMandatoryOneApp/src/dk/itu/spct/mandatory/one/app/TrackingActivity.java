package dk.itu.spct.mandatory.one.app;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TrackingActivity extends Activity {
	private LocationFinder loc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tracking);
		
		String name = getIntent().getExtras().getString(MainActivity.MAIN_ACTIVITY_NAME_FIELD);
		String IP = getIntent().getExtras().getString(MainActivity.MAIN_ACTIVITY_IP_ADDRESS);

		loc = new LocationFinder(this, name, IP, this);

		Address add = getAddress();
		if (add != null) {
			loc.addProximityAlert(add.getLatitude(), add.getLongitude());
			Log.d("test", add.toString());
		} else {
			Toast.makeText(this,
					"No address found, check your internet connection",
					Toast.LENGTH_LONG).show();
		}

	}

	private Address getAddress() {

		Geocoder coder = new Geocoder(this);
		List<Address> address;

		Log.d("Geocoder", "Geocoder available = " + Geocoder.isPresent());

		try {
			address = coder.getFromLocationName("IT University of Copenhagen",
					5);
			if (address == null) {
				return null;
			}
			Address location = address.get(0);

			return location;

		} catch (IOException e) {

		}
		return null;

	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		loc.unRegisterReceiver();
	}
	
	/**
	 * get bluetooth adapter MAC address
	 * @return MAC address String
	 */
	public static String getBluetoothMacAddress() {
	    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	 
	    // if device does not support Bluetooth
	    if(mBluetoothAdapter==null){
	        Log.d(MainActivity.class.getName(), "device does not support bluetooth");
	        return null;
	    }
	     
	    return mBluetoothAdapter.getAddress();
	}

	public void setBackground(boolean atITU) {
		if(atITU){
			// Set green Background color
			((LinearLayout)findViewById(dk.itu.spct.mandatory.one.app.R.id.tracking_container)).setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
			((TextView) findViewById(R.id.trackingText)).setText("You are at ITU");
		} else {
			// Set red Background color
			((LinearLayout)findViewById(dk.itu.spct.mandatory.one.app.R.id.tracking_container)).setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
			((TextView) findViewById(R.id.trackingText)).setText("You are NOT at ITU");
		}
		
	}

}
