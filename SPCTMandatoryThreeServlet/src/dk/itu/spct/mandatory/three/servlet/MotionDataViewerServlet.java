package dk.itu.spct.mandatory.three.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;

public class MotionDataViewerServlet extends HttpServlet {
	private final String NEWLINE = "\r\n";
	public static final String ACTIVITY_TYPE_KEY = "type";
	public static final String ACTIVITY_TYPE_TEST_KEY = "test";
	public static final String ACTIVITY_TYPE_TRAINING_KEY = "training";
	public static final String ID_KEY = "id";
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		resp.setContentType("text/plain");
		resp.setCharacterEncoding("UTF-8");
		PrintWriter writer = resp.getWriter();
		
		String activityType = req.getParameter(ACTIVITY_TYPE_KEY);
		
		if(activityType == null) {
			// output error
			writer.print("No type specified");
			return;
		}
		
		if(activityType.equals(ACTIVITY_TYPE_TRAINING_KEY)) {
			// output everything with type = training
			Query query = new Query(ACTIVITY_TYPE_TRAINING_KEY);
			List<Entity> result = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(20));
			
			for(int i = 0; i < result.size(); i++) {
				String raw = ((Text) result.get(i).getProperty(MotionPostServlet.DATA_KEY)).getValue();
				// remove trailing whitespace
				raw = raw.trim();
				// remove header from all entities but the first
				if(i>0) {
					raw = raw.substring(28);
				}
				writer.print(raw);
			}
			
			resp.setStatus(HttpServletResponse.SC_OK);
		} else if(activityType.equals(ACTIVITY_TYPE_TEST_KEY)) {
			// get id param
			String id = req.getParameter(ID_KEY);
			
			if(id != null) {
				// output sample with matching id
//				id = id.toLowerCase();
				
				Query query = new Query(ACTIVITY_TYPE_TEST_KEY);
				List<Entity> result = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(20));
				
				for(Entity entity : result) {
					// only print when id (name) matches
					String entityName = (String) entity.getProperty(MotionPostServlet.NAME_KEY);
					
					if(entityName.equals(id)) {
						String s = ((Text) entity.getProperty(MotionPostServlet.DATA_KEY)).getValue() + NEWLINE;
						writer.print(s);
						writer.print(NEWLINE + NEWLINE);
					}
				}
			} else {
				// output error
				writer.print("No id specified.");
			}
		} else {
			// output error
			// or maybe output everything with type = training?
			writer.print("Type not recognized.");
		}
		
		
		
//		Query query = new Query(MotionPostServlet.DATA_KIND);
//		query.addSort(MotionPostServlet.NAME_KEY, Query.SortDirection.ASCENDING);
//		List<Entity> result = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(10));
//		
		
		
		
		
		
	}

}
