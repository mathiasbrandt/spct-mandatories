package dk.itu.spct.mandatory.three.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;

public class MotionPostServlet extends HttpServlet {
	public static final String KEY_NAME = "DataSampleKey";
	
	public static final String NAME_KEY = "name";
	public static final String TYPE_KEY = "type";
	public static final String DATA_KEY = "data";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = (String) req.getParameter(NAME_KEY);
		String type = (String) req.getParameter(TYPE_KEY);
		String data = (String) req.getParameter(DATA_KEY);
		
//		resp.getWriter().write(name + ", " + type + ", " + data);
		
		if(name == null || type == null || data == null) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		type = type.toLowerCase();
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Key key = KeyFactory.createKey(type, KEY_NAME);
		Entity entity = new Entity(key.getKind(), key);
		
		entity.setProperty(NAME_KEY, name);
		entity.setProperty(TYPE_KEY, type);
		entity.setProperty(DATA_KEY, new Text(data));
		datastore.put(entity);
		
		resp.setStatus(HttpServletResponse.SC_OK);
	}
	
}
