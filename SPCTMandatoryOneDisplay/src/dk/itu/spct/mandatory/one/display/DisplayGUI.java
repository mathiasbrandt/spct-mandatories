package dk.itu.spct.mandatory.one.display;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class DisplayGUI extends JFrame {
	private static final long serialVersionUID = 1L;
	private final String WINDOW_TITLE;
	private final String SERVICE_URI;
	private String id;
	
	private JScrollPane scrollPane;
	private JTable tableDevices;
	private Thread personMonitorThread;
	
	/*
	 * TODO:
	 * Implement ContextChanged
	 * Get info about device in front of display
	 * Refresh display to show info relevant to device
	 */

	public DisplayGUI(String id, String service_uri) {
		SERVICE_URI = service_uri;
		WINDOW_TITLE = "Display at zone '" + id + "'";
		this.id = id;
		
		createGUI();
		createMonitor();
	}
	
	private void createGUI() {
		this.setSize(800, 600);
		this.setTitle(WINDOW_TITLE);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Device ID");
		model.addColumn("Person Name");
		
		// add dummy data
		model.addRow(new String[] { "98:0d:2e:46:ce:82".toUpperCase(), "Mathias" });
		model.addRow(new String[] { "84:7a:88:7a:38:e2".toUpperCase(), "Simon" });
		
		tableDevices = new JTable(model);
		
		scrollPane = new JScrollPane(tableDevices);
		scrollPane.setSize(this.getSize());
		
		this.add(scrollPane);
		this.setVisible(true);
	}
	
	private void createMonitor() {
		PersonMonitor personMonitor = new PersonMonitor(SERVICE_URI, id, tableDevices);
		personMonitorThread = new Thread(personMonitor);
		personMonitorThread.start();
	}

	public static void main(String[] args) {
		new DisplayGUI("itu.zone4.zone4c", "rmi://10.25.215.245/aware");
		//new DisplayGUI("itu.zone4.zone4c", "rmi://192.168.1.108/aware");
	}
}