package dk.itu.spct.mandatory.one.display;

import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dk.itu.spct.mandatory.one.contextItems.Location;
import dk.itu.spct.mandatory.one.entities.Person;
import dk.pervasive.jcaf.ContextEvent;
import dk.pervasive.jcaf.EntityListener;
import dk.pervasive.jcaf.impl.RemoteEntityListenerImpl;
import dk.pervasive.jcaf.util.AbstractContextClient;

public class PersonMonitor extends AbstractContextClient {
	private String displayId;
	private DefaultTableModel dataModel;
	private Boolean isRunning = false;
	private ArrayList<Person> currentDevices;
	
	public PersonMonitor(String service_uri, String displayId, JTable data) {
		super(service_uri);
		
		this.displayId = displayId;
		dataModel = (DefaultTableModel) data.getModel();
		currentDevices = new ArrayList<Person>();
	}

	@Override
	public void run() {
		System.out.println("Starting PersonMonitor...");
		isRunning = true;
		
		addPersonListener();
		loop();
	}

	public void stop() {
		System.out.println("Stopping PersonMonitor...");
		isRunning = false;
	}
	
	private void loop() {
		while(isRunning) {
			try {
				refreshDeviceList();
				Thread.sleep(5000);
			} catch(InterruptedException e) {
				System.out.println("Thread interrupted.");
			}
		}
	}
	
	private void addPersonListener() {
		try {
			RemoteEntityListenerImpl personListener = new RemoteEntityListenerImpl();
			personListener.addEntityListener(new EntityListener() {
				@Override
				public void contextChanged(ContextEvent event) {
					Person person = (Person) event.getEntity();
					Location location = (Location) event.getItem();
					
					// if the person's location matches the location of this display
					if(displayId.equals(location.getId())) {
						System.out.println(String.format("%s (%s) has moved to %s", person.getName(), person.getId(), location.getLocation()));
						currentDevices.add(person);
					} else {
						if(currentDevices.contains(person)) {
							System.out.println("Removing " + person.getId());
							currentDevices.remove(person);
						}
					}
				}
			});
			
			getContextService().addEntityListener(personListener, Person.class);
		} catch (RemoteException e) {
			System.out.println("Error while adding entity listeners");
			e.printStackTrace();
		}
	}
	
	private void refreshDeviceList() {
		dataModel.setRowCount(0);
		
		for(Person person : currentDevices) {
			addDataRow(person.getId(), person.getName());
		}
	}
	
	private void addDataRow(String id, String name) {
		dataModel.addRow(new String[] { id, name });
	}
}
