import java.net.MalformedURLException;
import java.rmi.*;

import dk.pervasive.jcaf.ContextService;
import dk.pervasive.jcaf.util.AbstractContextClient;

public class Main extends AbstractContextClient {
	public static String SERVICE_URI = "rmi://localhost/aware";
	private MyEntity testEntity;
	
	public Main(String service_uri) {
		super(service_uri);
		
		load();
		test();
	}
	
	
	public void load() {
		try {
			testEntity = new MyEntity("my id");
			getContextService().addEntity(testEntity);
			testEntity.setName("derp");
			getContextService().setEntity(testEntity);
			
			System.out.println(getContextService().getAllEntityIds().length);
			
			getContextService().removeEntity(testEntity);
		} catch (RemoteException e) {
			System.out.println("Error in load: " + e.getMessage());
		}
	}
	
	public void test() {
		
	}

	@Override
	public void run() {
		System.out.println("run");
	}
	
	public static void main(String[] args) {
		new Main(SERVICE_URI);
	}
}
