import dk.pervasive.jcaf.ContextEvent;
import dk.pervasive.jcaf.EntityConfig;
import dk.pervasive.jcaf.entity.GenericEntity;


public class MyEntity extends GenericEntity {
	private static final long serialVersionUID = -4708924039278022442L;
	private String name;
	
	public MyEntity() {
		super();
	}
	
	public MyEntity(String id) {
		super(id);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public void contextChanged(ContextEvent event) {
		super.contextChanged(event);
		
		System.out.println("ContextChanged");
	}

	@Override
	public void destroy() {
		super.destroy();
		
		System.out.println("Destroy");
	}

	@Override
	public void init(EntityConfig config) {
		super.init(config);
		
		System.out.println("Init");
	}
	
	public String toXML() {
		return "lol";
	}
}
