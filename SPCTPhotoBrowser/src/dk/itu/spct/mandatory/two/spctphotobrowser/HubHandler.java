package dk.itu.spct.mandatory.two.spctphotobrowser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.concurrent.Executor;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

/***
 * Contains methods to be invoked from the server through the hub.
 */
public class HubHandler {
	private final String TAG = "HubHandler";
	private MainActivity context;
	private String webApiBaseAddress;
	private String phoneId;

	public HubHandler(MainActivity context, String webApiBaseAddress, String phoneId) {
		this.context = context;
		this.webApiBaseAddress = webApiBaseAddress;
		this.phoneId = phoneId;
	}
	
	/***
	 * A request from the server to download a single image from the specified URL.
	 * This method is invoked when an image is dropped on the phone on the Surface GUI.
	 */
	public void downloadImageFromServer(String url, String targetId, String imageId) {
		// download image (probably in an AsyncTask)
		// save image to STORAGE_DIR (maybe create a static method in MainActivity, like GetGalleryImages?)
		// refresh gallery (done via method in MainActivity (static?))
		AsyncTask<String, Void, Boolean> downloadTask = new DownloadAsyncTask().execute(url, targetId, imageId);
	}
	
	/***
	 * A request from the server to upload all images on this device to the specified POST URL.
	 * This method is automatically invoked by the server when the device has identified itself.
	 * The upload is done in the background.
	 */
	public void uploadImagesToServer(String url) {
		final String finalUrl = url;
		context.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				new UploadAsyncTask().execute(finalUrl);
			}
		});
	}
	
	class DownloadAsyncTask extends AsyncTask<String, Void, Boolean> {
		@Override
		protected Boolean doInBackground(String... params) {
			// 0 = url
			// 1 = targetId
			// 2 = imageId
			Boolean success = download(params[0], params[1], params[2]);
			
			return success;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			if(result) {
				Log.d(TAG, "Image downloaded successfully");
				Toast.makeText(context, "A new image has been downloaded", Toast.LENGTH_LONG).show();
				context.refreshGallery();
				context.informServerDownloadComplete();
			} else {
				Log.d(TAG, "An error occured while downloading an image");
				Toast.makeText(context, "An error occured while downloading an image", Toast.LENGTH_LONG).show();
			}
		}

		private Boolean download(String GETurl, String targetId, String imageId) {
			try {
				GETurl = String.format("%s/%s/%s/%s", webApiBaseAddress, GETurl, targetId, imageId);
				Log.d(TAG, "Downloading image from " + GETurl);
				
				URL url = new URL(GETurl);
				
				URLConnection conn = url.openConnection();
				conn.connect();
				Bitmap bitmap = BitmapFactory.decodeStream(conn.getInputStream());
				
				SpctImage spctImage = new SpctImage(imageId, bitmap);
				
				context.saveImageToStorage(spctImage);
				
				return true;
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return false;
		}
	}
	
	class UploadAsyncTask extends AsyncTask<String, Integer, Boolean> {
		ProgressDialog dialog;
		private int statusCode;
		
		@Override
		protected void onPreExecute() {
			// TODO: why does this not work? :S
			dialog = new ProgressDialog(context);
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			dialog.setTitle("Uploading images");
			dialog.show();
		}

		@Override
		protected Boolean doInBackground(String... url) {
			Boolean success = upload(url[0]);
			
			return success;
		}
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			dialog.setMessage(String.format("Uploaded %d of %d images", values[0], values[1]));
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			dialog.dismiss();
			
			if(result) {
				Log.d(TAG, "Images uploaded successfully");
				Toast.makeText(context, "Images uploaded successfully", Toast.LENGTH_LONG).show();
				context.informServerUploadComplete();
			} else {
				Log.d(TAG, String.format("An error occured while uploading images (%d)", statusCode));
				Toast.makeText(context, String.format("An error occured while uploading images (%d)", statusCode), Toast.LENGTH_LONG).show();
			}
		}
		
		private Boolean upload(String url) {
			try {
				url = String.format("%s/%s", webApiBaseAddress, url);
				Log.d(TAG, String.format("Uploading images to %s", url));
				
				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(url);
				
				MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
				entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
				
				entityBuilder.addTextBody("id", phoneId);
				
				ArrayList<SpctImage> images = context.getGalleryImages();
				
				for(int i = 0; i < images.size(); i++) {
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					images.get(i).getBitmap().compress(CompressFormat.JPEG, 100, outputStream);
					byte[] imageData = outputStream.toByteArray();
					
//					entityBuilder.addBinaryBody(String.format("image_%d", i + 1), imageData);
					entityBuilder.addBinaryBody(images.get(i).getName(), imageData);
					
					publishProgress(i + 1, images.size());
				}
				
				httpPost.setEntity(entityBuilder.build());
				
//				url = "http://10.25.229.217:9000/api/images/Simon";
//				url = "http://10.25.229.217:9000/api/images?phoneid=Simon";
//				Log.d(TAG, "Now URL is: " + url);
				HttpResponse httpResponse = httpClient.execute(httpPost, new BasicHttpContext());
				
				Log.d(TAG, "Server response: " + httpResponse.getStatusLine());
				
				statusCode = httpResponse.getStatusLine().getStatusCode();
				return statusCode == 200;
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return false;
		}
	}

}
