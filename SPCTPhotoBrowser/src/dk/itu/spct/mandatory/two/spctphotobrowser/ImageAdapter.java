package dk.itu.spct.mandatory.two.spctphotobrowser;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class ImageAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<SpctImage> images;
	private int columns;

	public ImageAdapter(Context c, int columnCount, ArrayList<SpctImage> data) {
		context = c;
		columns = columnCount;
		images = data;
	}

	@Override
	public int getCount() {
		return images.size();
	}

	@Override
	public Object getItem(int index) {
		return images.get(index);
	}

	@Override
	public long getItemId(int position) {
		return  (long) Math.ceil(position/columns);
	}
	
	public void updateData(ArrayList<SpctImage> data) {
		images = data;
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if(convertView == null) {
			imageView = new ImageView(context);
//			imageView.setLayoutParams(new GridView.LayoutParams(100, 100));
			imageView.setScaleType(ScaleType.CENTER_INSIDE);
			imageView.setPadding(0, 0, 0, 0);
		} else {
			imageView = (ImageView) convertView;
		}
		
		imageView.setImageBitmap(images.get(position).getBitmap());
		return imageView;
	}
}
