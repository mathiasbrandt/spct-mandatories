package dk.itu.spct.mandatory.two.spctphotobrowser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class SelectIdActivity extends Activity {
	public static final String PHONE_ID_KEY = "phoneId";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_id);
	}
	
	public void buttonMathiasOnClick(View v) {
		Intent i = new Intent(getApplicationContext(), MainActivity.class);
		i.putExtra(PHONE_ID_KEY, "Mathias");
		startActivity(i);
	}
	
	public void buttonSimonOnClick(View v) {
		Intent i = new Intent(getApplicationContext(), MainActivity.class);
		i.putExtra(PHONE_ID_KEY, "Simon");
		startActivity(i);
	}
}
