package dk.itu.spct.mandatory.two.spctphotobrowser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.GridView;

public class MainActivity extends Activity {
	private final static String TAG = "MainActivity";
	private static String STORAGE_DIR;
	private String PHONE_ID = "phoneId";
//	private final String SIGNALR_HOST_URL = "http://10.25.229.217:1337";
//	private final String WEBAPI_BASE_ADDRESS = "http://10.25.229.217:9000";
//	private final String SIGNALR_HOST_URL = "http://10.25.235.168:1337";
//	private final String WEBAPI_BASE_ADDRESS = "http://10.25.235.168:9000";
	private final String SIGNALR_HOST_URL = "http://10.6.6.192:1337";			// Surface @ PITLab
	private final String WEBAPI_BASE_ADDRESS = "http://10.6.6.192:9000";		// Surface @ PITLab
//	private final String SIGNALR_HOST_URL = "http://192.168.1.80:1337";
//	private final String WEBAPI_BASE_ADDRESS = "http://192.168.1.80:9000";
	private final String HUB_NAME = "PhoneHub";
	private HubConnection hubConnection;
	private HubProxy hub;
	private GridView gridViewImages;
	private ImageAdapter galleryAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		PHONE_ID = this.getIntent().getStringExtra(SelectIdActivity.PHONE_ID_KEY);
		
		STORAGE_DIR = Environment.getExternalStorageDirectory() + "/SPCTPhotoBrowser" + "/sample_images";
		
		/* set up gallery */
		gridViewImages = (GridView) findViewById(R.id.gridViewImages);
		populateGridLayout();
		
		/* connect to server and identify */
		Boolean success = connectToServer();
		if(success) {
			identifyToServer();
		} else {
			AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
			dialogBuilder.setTitle("Connection failed");
			dialogBuilder.setMessage("A connection to the server could not be established");
			dialogBuilder.setPositiveButton("OK", null);
			dialogBuilder.show();
		}
	}
	
	/***
	 * Creates a connection to the server and initializes a hub proxy.
	 * Subscribes a HubHandler to handle methods invoked by the server through the hub.
	 * Returns true if connection was established, false otherwise.
	 */
	private Boolean connectToServer() {
		Log.d(TAG, "Connecting to server");
		
		// this is apparently necessary for some unexplained reason :P
		Platform.loadPlatformComponent(new AndroidPlatformComponent());
		
		try {
			hubConnection = new HubConnection(SIGNALR_HOST_URL);
			hub = hubConnection.createHubProxy(HUB_NAME);
			hubConnection.start().get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			Log.d(TAG, "Could not connect to host. Check your firewall settings.");
			e.printStackTrace();
			
			return false;
		}
		
		hub.subscribe(new HubHandler(this, WEBAPI_BASE_ADDRESS, PHONE_ID));
		
		return true;
	}
	
	/***
	 * Identifies the phone to the server by sending its ID.
	 */
	private void identifyToServer() {
		Log.d(TAG, "Identifying on server");
		hub.invoke("Identify", PHONE_ID);
	}
	
	/***
	 * Populates the grid in the GUI with images from STORAGE_DIR.
	 */
	private void populateGridLayout() {
		ArrayList<SpctImage> images = getGalleryImages();
		
		galleryAdapter = new ImageAdapter(this, gridViewImages.getNumColumns(), images);
		gridViewImages.setAdapter(galleryAdapter);
	}
	
	/***
	 * Loads images from STORAGE_DIR and returns them in a list.
	 */
	public ArrayList<SpctImage> getGalleryImages() {
		File imageDir = new File(STORAGE_DIR);
		File[] files = imageDir.listFiles();
		Log.d(TAG, String.format("Found %d images in %s", files.length, STORAGE_DIR));
		
		ArrayList<SpctImage> images = new ArrayList<SpctImage>();
		for(int i = 0; i < files.length; i++) {
			Bitmap bitmap = BitmapFactory.decodeFile(files[i].getAbsolutePath());
			if(bitmap != null) {
//				Log.d(TAG, "filename: " + files[i].getName());
				SpctImage spctImage = new SpctImage(files[i].getName(), bitmap);
				images.add(spctImage);
			}
		}
		
		return images;
	}
	
	public void refreshGallery() {
		Log.d(TAG, "Refreshing gallery");
		galleryAdapter.updateData(getGalleryImages());
	}
	
	public void informServerDownloadComplete() {
		Log.d(TAG, "Invoking DownloadComplete on hub");
		hub.invoke("DownloadComplete", PHONE_ID);
	}
	
	public void informServerUploadComplete() {
		Log.d(TAG, "Invoking UploadComplete on hub");
		hub.invoke("UploadComplete", PHONE_ID);
	}
	
	public void saveImageToStorage(SpctImage newImage) {
		try {
			File imageDir = new File(STORAGE_DIR);
			File[] existingImages = imageDir.listFiles();
			
			Boolean go = true;
			while(go) {
				for(int i = 0; i < existingImages.length; i++) {
					if(existingImages[i].getName().equals(newImage.getName())) {
						Log.d(TAG, String.format("%s already found. Renaming.", newImage.getName()));
						newImage.setName("_" + newImage.getName());
						
						go = true;
						break;
					}
					
					go = false;
				}
			}
			
			File newImageFile = new File(String.format("%s/%s", imageDir, newImage.getName()));
			
			
			
			Log.d(TAG, "Saving to " + newImageFile.getPath());
			FileOutputStream outputStream  = new FileOutputStream(newImageFile);
			newImage.getBitmap().compress(CompressFormat.JPEG, 100, outputStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
