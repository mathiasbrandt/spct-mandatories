package dk.itu.spct.mandatory.two.spctphotobrowser;

import android.graphics.Bitmap;

public class SpctImage {
	private String name;
	private Bitmap image;

	public SpctImage(String name, Bitmap image) {
		this.name = name;
		this.image = image;
	}
	
	public Bitmap getBitmap() {
		return image;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
