package dk.itu.spct.mandatory.three.app;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

public class SamplingService extends Service implements SensorEventListener {

	private final int rate = 50000;
	private SensorManager sensorManager;
	private Sensor accelerometer;
	private boolean mInitialized;

	private String dataString;
	private String sampleName;
	private String activityTypeString;
	private String sampleTypeString;

	private StringBuilder builder;

	private final Handler waitHandler = new Handler();
	private final Handler startHandler = new Handler();

	private final SamplingServiceBinding binding = new SamplingServiceBinding();

	private static final String newLine = System.getProperty("line.separator");

	@Override
	public void onCreate() {
		super.onCreate();

		// get access to accelerometer service
		sensorManager = (SensorManager) getSystemService(Service.SENSOR_SERVICE);
		accelerometer = sensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

	}

	@Override
	public IBinder onBind(Intent intent) {
		return this.binding;
	}

	public class SamplingServiceBinding extends Binder {

		public void startSampling(String name, String sampleType,
				String activityType) {

			sampleName = name;

			sampleTypeString = sampleType;

			// append Headers for CSV file
			activityTypeString = activityType;

			builder = new StringBuilder();

			builder.append("Timestamp, X, Y, Z");
			if (!activityTypeString.equals("none")) {
				builder.append(", Activity");
			}

			builder.append("\n");

			// Wait 10 seconds before sampling
			waitHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					// Save name
					sensorManager.registerListener(SamplingService.this,
							accelerometer, rate);
					// Stop Sampling after 10 seconds
					startHandler.postDelayed(new Runnable() {
						@Override
						public void run() {
							stopSampling();
						}
					}, 10000);
				}
			}, 10000);
		}

		public void stopSampling() {
			sensorManager.unregisterListener(SamplingService.this);

			dataString = builder.toString();
		}

		public void uploadData() {
			new UploadAsyncTask().execute();
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		float x = event.values[0];
		float y = event.values[1];
		float z = event.values[2];

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
				Locale.getDefault());
		String formattedDate = df.format(c.getTime());

		if (!mInitialized) {
			mInitialized = true;
		} else {
			// save data
			builder.append("\"" + formattedDate + "\"" + ", " + x + ", " + y
					+ ", " + z);

			if (!activityTypeString.equals("none")) {
				builder.append(", " + activityTypeString);
			}

			builder.append("\n");

		}
	}

	class UploadAsyncTask extends AsyncTask<String, Integer, Boolean> {
		private int statusCode;
		
		@Override
		protected Boolean doInBackground(String... arg0) {
			if (mInitialized) {

				String url = "http://spctmotion.appspot.com/motionpost";
				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(url);
				httpPost.setHeader("Content-Type",
						"application/x-www-form-urlencoded");

				if (sampleName.isEmpty()) {
					sampleName = "Unnamed Sample";
				}

				try {
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
							2);
					nameValuePairs.add(new BasicNameValuePair("name",
							sampleName));
					nameValuePairs.add(new BasicNameValuePair("type",
							sampleTypeString.toLowerCase()));
					nameValuePairs.add(new BasicNameValuePair("data",
							dataString));
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

					// Execute HTTP Post Request
					statusCode = httpClient.execute(httpPost)
							.getStatusLine().getStatusCode();
				} catch (ClientProtocolException e) {
					// Catch Protocol Exception
				} catch (IOException e) {
					// Catch IOException
				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if(statusCode == HttpStatus.SC_OK) {
				Toast.makeText(getApplicationContext(), "Data successfully uploaded", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(getApplicationContext(), String.format("An error occured while uploading data (%d)", statusCode), Toast.LENGTH_LONG).show();
			}
		}
	}

}