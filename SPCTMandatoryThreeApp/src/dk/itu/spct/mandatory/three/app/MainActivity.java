package dk.itu.spct.mandatory.three.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.spctmandatorythreeapp.R;

import dk.itu.spct.mandatory.three.app.SamplingService.SamplingServiceBinding;

public class MainActivity extends Activity {
	private SamplingServiceBinding service;

	private boolean bound = false;

	private final ServiceConnection serviceConn = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			MainActivity.this.bound = false;
			MainActivity.this.service = null;
		}

		@Override
		public void onServiceConnected(ComponentName classInfo,
				IBinder serviceBinding) {
			if (serviceBinding instanceof SamplingServiceBinding) {
				// Successfully bound to the movement service.
				MainActivity.this.service = (SamplingServiceBinding) serviceBinding;
				MainActivity.this.bound = true;
			}
		}
	};

	private boolean isInterrupted;
	private String counterText;
	private CountDownTimer countWaiting;
	private CountDownTimer countFinish;

	private final Handler waitHandler = new Handler();
	private final Handler stopHandler = new Handler();

	private TextView txtCountdown;
	private Button btnStartSampling;
	private Button btnStopSampling;
	private Button btnUpload;
	private EditText edtSampleName;
	private RadioGroup rdgSampleType;
	private Spinner spnActivityType;

	private WakeLock wl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "My Tag");

		String[] activityTypes = new String[] { "none", "walking", "sitting",
				"climbing" };

		spnActivityType = (Spinner) findViewById(R.id.spnActivityType);
		spnActivityType.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, activityTypes));

		btnStartSampling = (Button) findViewById(R.id.btnStartSampling);
		btnStopSampling = (Button) findViewById(R.id.btnStopSampling);
		btnUpload = (Button) findViewById(R.id.btnUpload);
		edtSampleName = (EditText) findViewById(R.id.edtSampleName);

		btnStopSampling.setEnabled(false);
		btnUpload.setEnabled(false);

		rdgSampleType = (RadioGroup) findViewById(R.id.rdgSampleType);

		txtCountdown = (TextView) findViewById(R.id.txtCountdown);
		countWaiting = new CountDownTimer(10000, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
				if (!isInterrupted) {
					txtCountdown.setText(counterText + millisUntilFinished
							/ 1000 + " seconds");
				} else {
					txtCountdown.setText("Sampling Stopped!");
					this.cancel();
				}
			}

			@Override
			public void onFinish() {
				if (!isInterrupted) {
					Vibrator v = (Vibrator) MainActivity.this
							.getSystemService(Context.VIBRATOR_SERVICE);
					// Vibrate for 500 milliseconds
					v.vibrate(500);
				}
			}
		};

		countFinish = new CountDownTimer(10000, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
				if (!isInterrupted) {
					txtCountdown.setText(counterText + millisUntilFinished
							/ 1000 + " seconds");
				} else {
					txtCountdown.setText("Sampling Stopped!");
					this.cancel();
				}
			}

			@Override
			public void onFinish() {
				if (!isInterrupted) {
					txtCountdown.setText("Sampling Finished");

					Vibrator v = (Vibrator) MainActivity.this
							.getSystemService(Context.VIBRATOR_SERVICE);
					// Vibrate for 500 milliseconds
					v.vibrate(500);
				}
			}
		};

		btnStartSampling.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				wl.acquire();

				isInterrupted = false;
				if (!bound) {
					Toast.makeText(MainActivity.this,
							"The recording service is currently unavailable.",
							Toast.LENGTH_SHORT).show();
					return;
				}

				// Start accelerometer recording service in case it was stopped.
				MainActivity.this.startService(new Intent(MainActivity.this,
						SamplingService.class));

				btnStartSampling.setEnabled(false);
				btnStopSampling.setEnabled(true);
				edtSampleName.setEnabled(false);
				btnUpload.setEnabled(false);

				for (int i = 0; i < rdgSampleType.getChildCount(); i++) {
					rdgSampleType.getChildAt(i).setEnabled(false);
				}

				spnActivityType.setEnabled(false);

				String sampleName = edtSampleName.getText().toString();

				String sampleType = ((RadioButton) findViewById(rdgSampleType
						.getCheckedRadioButtonId())).getText().toString();

				String activityType = (String) spnActivityType
						.getSelectedItem();

				// Wait 10 seconds before sampling
				waitHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						counterText = "Sampling ends in ";
						countFinish.start();

						// Stop Sampling after 10 seconds
						stopHandler.postDelayed(new Runnable() {
							@Override
							public void run() {
								stopSampling();
							}
						}, 10000);
					}
				}, 10000);

				counterText = "Sampling starts in ";
				countWaiting.start();

				MainActivity.this.service.startSampling(sampleName, sampleType,
						activityType);
			}
		});

		btnStopSampling.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				isInterrupted = true;
				stopSampling();
				wl.release();
			}
		});
	}

	public void stopSampling() {
		btnStartSampling.setEnabled(true);
		btnStopSampling.setEnabled(false);

		edtSampleName.setEnabled(true);
		spnActivityType.setEnabled(true);

		for (int i = 0; i < rdgSampleType.getChildCount(); i++) {
			rdgSampleType.getChildAt(i).setEnabled(true);
		}

		if (!isInterrupted) {
			btnUpload.setEnabled(true);
		}

		// Stop all delayed callback
		stopHandler.removeCallbacksAndMessages(null);
		waitHandler.removeCallbacksAndMessages(null);

		if (isInterrupted) {
			countWaiting.cancel();
			countFinish.cancel();
		}

		MainActivity.this.service.stopSampling();
	}

	@Override
	protected void onStart() {
		super.onStart();
		/*
		 * Start the service
		 */
		ComponentName serviceName = this.startService(new Intent(this,
				SamplingService.class));
		/*
		 * Bind to service to get an interface for the service. This interface
		 * is delivered to the ServiceConnection implementation.
		 */
		Intent i = new Intent(this, SamplingService.class);
		this.bindService(i, this.serviceConn, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (bound) {
			/*
			 * Unbind from the service but do not stop the service.
			 */
			this.unbindService(this.serviceConn);
			this.bound = false;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void uploadData(View v) {
		btnUpload.setEnabled(false);
		edtSampleName.setEnabled(true);

		service.uploadData();
	}

}
