package dk.itu.spct.mandatory.one.monitors;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import dk.itu.spct.mandatory.one.contextItems.Location;
import dk.itu.spct.mandatory.one.relations.Located;
import dk.pervasive.jcaf.Context;
import dk.pervasive.jcaf.ContextService;
import dk.pervasive.jcaf.util.AbstractContextClient;

/***
 * Monitors persons when they move around ITU (using the BLIP system.)
 */
public class BLIPMonitor extends AbstractContextClient {
	private ContextService contextService;
	private TrackerService trackerService;
	private Thread monitorThread;
	private Boolean isRunning;

	public BLIPMonitor(String service_uri) {
		super(service_uri);
		
		contextService = getContextService();
		trackerService = new TrackerService();
	}

	@Override
	public void run() {
		isRunning = true;
		listen();
	}
	
	public void stop() {
		isRunning = false;
	}
	
	/***
	 * Listens for updates from the BLIP system about people's movements.
	 */
	private void listen() {
		/*
		 * flow:
		 * new thread
		 * while isRunning
		 * * get list of ids from context service
		 * * for each id ...
		 * * * check location of id in BLIP
		 * * * check if location from BLIP is different than current location (from context service)
		 * * * if true: update location
		 * * * if false: do nothing
		 */
		
		monitorThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while(isRunning) {
					try {
						ArrayList<String> ids = new ArrayList<String>(Arrays.asList(contextService.getAllEntityIds()));
						
						// loop through all ids currently registered with the context service.
						for(String id : ids) {
							Context context = contextService.getContext(id);
							// TODO: what is 'source' parameter?
							Located located = new Located("source");
							
							// retrieve the current location from BLIP
							Location currentLocation = trackerService.getDeviceLocation(id);
							
							if(context.containsRelationship(located)) {
								// get the last known location from the context service
								Location oldLocation = (Location) context.getContextItem(located);
								
								// if current location does not match the last known location, update the location in the context service
								if(!oldLocation.getId().equals(currentLocation.getId())) {
									System.out.println(String.format("Updating location for '%s' from '%s' to '%s'", id, oldLocation, currentLocation));
									contextService.addContextItem(id, located, currentLocation);
								}
							}
							else {
								System.out.println("Located relation not found, must be a new arrival. Adding location: " + currentLocation.getLocation());
								contextService.addContextItem(id, located, currentLocation);
							}
						}
						
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
		monitorThread.start();
	}
}
