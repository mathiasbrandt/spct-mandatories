package dk.itu.spct.mandatory.one.monitors;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import dk.itu.spct.mandatory.one.entities.Person;
import dk.pervasive.jcaf.ContextService;
import dk.pervasive.jcaf.util.AbstractContextClient;

/***
 * Monitors who arrives at, and leaves from ITU.
 */
public class PersonMonitor extends AbstractContextClient {
	private final int SERVER_SOCKET_PORT = 1337;
	private final String KEY_NAME = "name";
	private final String KEY_BLUETOOTH_ID = "bluetoothId";
	private final String KEY_IS_ARRIVING = "isArriving";
	
	private ContextService contextService;
	private Boolean isRunning;
	
	public PersonMonitor(String service_uri) {
		super(service_uri);
		
		contextService = getContextService();
	}

	@Override
	public void run() {
		isRunning = true;
		listen();
	}
	
	public void stop() {
		isRunning = false;
	}
	
	/***
	 * Listens for when people arrive and leave.
	 */
	private void listen() {
		try {
			System.out.println("PersonMonitor: listening...");
			
			ServerSocket serverSocket = new ServerSocket(SERVER_SOCKET_PORT);
			serverSocket.setSoTimeout(1000);
			
			while(isRunning) {
				Socket socket = null;
				
				try {
					/* Accept incoming connections.
					 * This is a blocking call, so a timeout is needed to be able the stop the thread (via isRunning) */
					socket = serverSocket.accept();
				} catch (SocketTimeoutException e) {
					// socket timed out, just continue
				}
				
				if(socket != null) {
					BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
					byte[] read = new byte[1024];
					bis.read(read);
					
					Person person = parseInput(read);
					
					handlePerson(person);
					
					socket.close();
				}
			}
			
			serverSocket.close();
			
			System.out.println("PersonMonitor: stopping.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/***
	 * Creates a Person object from received JSON input.
	 */
	private Person parseInput(byte[] input) {
		try {
			String jsonString = new String(input);
			JSONObject jsonObject = new JSONObject(jsonString);

			String id = jsonObject.getString(KEY_BLUETOOTH_ID);
			String name = jsonObject.getString(KEY_NAME);
			Boolean isArriving = jsonObject.getBoolean(KEY_IS_ARRIVING);
			
			return new Person(id, name, isArriving);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/***
	 * Checks if the person is arriving or leaving and updates the context service accordingly.
	 */
	private void handlePerson(Person person) {
		try {
			ArrayList<String> ids = new ArrayList<String>(Arrays.asList(contextService.getAllEntityIds()));
			
			if(person.isArriving()) {
				if(!ids.contains(person.getId())) {
					System.out.println(String.format("%s (%s) has arrived.", person.getName(), person.getId()));
					contextService.addEntity(person);
				}
				else {
					System.out.println(String.format("Error: %s (%s) has already arrived.", person.getName(), person.getId()));
				}
			}
			else {
				if(ids.contains(person.getId())) {
					System.out.println(String.format("%s (%s) has left.", person.getName(), person.getId()));
					contextService.removeEntity(person.getId());
				}
				else {
					System.out.println(String.format("Error: %s (%s) has already left, or never arrived.", person.getName(), person.getId()));
				}
			}
		} catch(RemoteException e) {
			e.printStackTrace();
		}
	}
}
