package dk.itu.spct.mandatory.one.monitors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import dk.itu.spct.mandatory.one.contextItems.Location;

public class TrackerService {
	private final String BASE_URL = "http://pit.itu.dk:7331/location-of/";
	private static final String LOCATION_NOT_AVAILABLE = "Not available";
	private final String KEY_LOCATION = "location";
	private final String KEY_ERROR = "error";
	private final int UPDATE_FREQUENCY = 1000;
	private Boolean isTracking = false;
	
	public TrackerService() {
		
	}
	
	/***
	 * Retrieve the current location of the device with the specified id.
	 */
	public Location getDeviceLocation(String deviceId) {
		String requestUrlString = BASE_URL + deviceId;
		String response = "";
		
		try {
			// contact BLIP to get location
			URL url = new URL(requestUrlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			
			int responseCode = conn.getResponseCode();
			
			if(responseCode == 200) {
				// read the response
				BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String temp;
				
				while((temp = reader.readLine()) != null) {
					response += temp;
				}
			}
			else {
				System.out.println("An error occured while contacting BLIP service.");
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return parseJsonForLocation(response);
	}
	
	/***
	 * Creates a location object based on the JSON input.
	 */
	private Location parseJsonForLocation(String jsonLocation) {
		JSONObject json = null;
		
		try {
			json = new JSONObject(jsonLocation);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		if(json != null) {
			try {
				if(!json.has(KEY_ERROR)) {
					return new Location(json.getString(KEY_LOCATION), json.getString(KEY_LOCATION));
				}
				else {
					//System.out.println(String.format("An error occured: %s", json.getString(KEY_ERROR)));
				}
			} catch (JSONException e) {
				System.out.println("Mapping does not exist.");
				e.printStackTrace();
			}
		}
		
		return new Location(LOCATION_NOT_AVAILABLE, LOCATION_NOT_AVAILABLE);
	}
}