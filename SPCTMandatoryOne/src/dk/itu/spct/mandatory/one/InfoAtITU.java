package dk.itu.spct.mandatory.one;

import dk.itu.spct.mandatory.one.monitors.BLIPMonitor;
import dk.itu.spct.mandatory.one.monitors.PersonMonitor;

/***
 * Main class for the context-aware system called Info @ ITU.
 */
public class InfoAtITU {
	private final String SERVICE_URI;
	private Thread personMonitorThread;
	private Thread blipMonitorThread;
	private PersonMonitor personMonitor;
	private BLIPMonitor blipMonitor;

	public InfoAtITU(String serviceUri) {
		SERVICE_URI = serviceUri;
		createMonitors();
	}
	
	/***
	 * Creates two monitors:
	 * PersonMonitor is used for tracking when people arrive to, and leave from, ITU.
	 * BLIPMonitor is used to track a person's movements inside ITU.
	 */
	private void createMonitors() {
		personMonitor = new PersonMonitor(SERVICE_URI);
		personMonitorThread = new Thread(personMonitor);
		personMonitorThread.start();
		
		blipMonitor = new BLIPMonitor(SERVICE_URI);
		blipMonitorThread = new Thread(blipMonitor);
		blipMonitorThread.start();
	}
	
	private void stopMonitors() {
		personMonitor.stop();
		blipMonitor.stop();
	}
	
	public static void main(String[] args) {
		new InfoAtITU("rmi://10.25.215.245/aware");
		//new InfoAtITU("rmi://192.168.1.108/aware");
	}
}
