package dk.itu.spct.mandatory.one.contextItems;

public class Location extends dk.pervasive.jcaf.item.Location {

	public Location() {
		super();
	}
	
	public Location(String id) {
		super(id);
	}
	
	public Location(String id, String location) {
		super(id, location);
	}
	
	@Override
	public String toString() {
		return getId();
	}
}
