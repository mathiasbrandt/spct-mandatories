package dk.itu.spct.mandatory.one.relations;

public class Located extends dk.pervasive.jcaf.relationship.Located {
	public Located() {
		super();
	}

	public Located(String source) {
		super(source);
	}
}
