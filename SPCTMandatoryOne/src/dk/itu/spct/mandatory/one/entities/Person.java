package dk.itu.spct.mandatory.one.entities;

import dk.pervasive.jcaf.ContextEvent;
import dk.pervasive.jcaf.ContextItem;

public class Person extends dk.pervasive.jcaf.entity.Person {
	private Boolean isArriving;
	
	public Person() {
		super();
	}
	
	public Person(String id) {
		super(id);
	}
	
	public Person(String id, String name) {
		super(id, name);
	}
	
	public Person(String id, String name, Boolean isArriving) {
		setName(name);
		setId(id);
		isArriving(isArriving);
	}
	
	public Boolean isArriving() {
		return isArriving;
	}
	
	public void isArriving(Boolean isArriving) {
		this.isArriving = isArriving;
	}
	
	@Override
	public String getEntityInfo() {
		return "Person entity";
	}

	@Override
	public void contextChanged(ContextEvent event) {
		super.contextChanged(event);
		
		Person person = (Person) event.getEntity();
		
		System.out.println(String.format("Context changed for %s (%s)", person.getName(), person.getId()));
	}

	@Override
	public boolean equals(Object obj) {
		if(getClass().equals(obj.getClass())) {
			return getId().equals(((Person) obj).getId());
		}
		
		return false;
	}
}
